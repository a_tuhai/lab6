import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";
import sun from "../images/sun-pic.png";
import { redirect } from "react-router-dom";

function Dashboard() {
  //використовуємо куки для збереження інформації про автентифікацію
  const [cookies, setCookie, removeCookie] = useCookies(["auth"]);
  //стан для збереження даних про погоду та міста
  const [weather, setWeather] = useState();
  const [location, setLocation] = useState("Kyiv");

  //вихід з облікового запису
  function logOut() {
    removeCookie("auth");
    //переходимо на сторінку логіну
    redirect("/");
  }

  //отримання даних про погоду за допомогою API
  async function fetchWeatherData(location) {
    try {
      const apiKey = "663d679e25314309910193340231411";

      const url = `https://api.weatherapi.com/v1/forecast.json?key=${apiKey}&q=${location}&days=5&aqi=no&alerts=no`;

      const response = await fetch(url);

      const data = await response.json();

      return data;
    } catch (error) {
      console.error("Error fetching data:", error);
      throw error;
    }
  }

  //useEffect для отримання геолокації користувача та завантаження даних про погоду за використанням цієї геолокації. 
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        fetchWeatherData(`${latitude},${longitude}`).then((weatherData) => {
          setWeather(weatherData);
        });
      },
      (error) => {
        console.error("Error getting geolocation:", error);
        fetchWeatherData("Kyiv").then((weatherData) => {
          setWeather(weatherData);
        });
      }
    );
  }, []);

  return weather ? (
    //відображення компонента, коли є дані про погоду
    <div className="dashboard-container">
      <header className="dashboard-header">
        <div className="header-content">
          <img className="sun-pic" src={sun} alt="Image of the sun" />
          <h1 className="weather-forecast">Weather forecast</h1>
        </div>
        <button className="log-out-button" type="submit" onClick={logOut}>
          Log out
        </button>
      </header>
      <main className="dashboard-main">
        <section className="upper-section">
          <div className="location-wrapper">
            <h1 className="location-info">
              <span className="city-main">{weather.location.name}, </span>
              <span className="country-main">{weather.location.country}</span>
            </h1>
            <section className="sunrise-sunset">
              <b>Sunrise: {weather.forecast.forecastday[0].astro.sunrise}</b>{" "}
              <br />
              <b>Sunset: {weather.forecast.forecastday[0].astro.sunset}</b>
            </section>
          </div>
        </section>

        <section className="figures-container">
          {weather.forecast.forecastday.map((day) => (
            <Figure key={day.date_epoch} data={day} />
          ))}
        </section>
      </main>
    </div>
  ) : (
    //відображення компонента завантаження, поки йдеться завантаження даних про погоду
    <Loader />
  );
}
function Loader() {
  return (
    <div className="loader-container">
      <div className="loader"></div>
    </div>
  );
}

//компонент для відображення деталей погоди на кожен день
function Figure(props) {
  //визначення дня тижня за заданою датою
  function defineDay(dateStr) {
    const dateArr = dateStr.split("-");
    //отримуємо поточну дату та час
    const today = new Date();
    const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    //перевіряємо, чи задана дата співпадає з поточною датою
    if (dateArr[2] == today.getDate()) {
      return "Today";
    } else if (dateArr[2] - today.getDate() === 1) {
      return "Tomorrow";
    } else {
      return (
        days[new Date(dateStr).getDay()] + " " + dateArr[2] + "." + dateArr[1]
      );
    }
  }

  const data = props.data;
  console.log(data);
  return (
    <figure className="figure">
      <div className="first-row">
        <h2 className="figure-day">{defineDay(data.date)}</h2>
        <img
          className="icon"
          src={data.day.condition.icon}
          alt="condition icon"
        />
      </div>
      <div className="condition-text">
        <b className="figure-text condition">{data.day.condition.text}</b>
      </div>

      <article className="weather-data">
        <p className="figure-text">
          Max: {data.day.maxtemp_c} C° <br />
        </p>
        <p className="figure-text">
          Min: {data.day.mintemp_c} C° <br />
        </p>
        <p className="figure-text">Wind speed: {data.day.maxwind_kph} km/h</p>
        <section className="weather-indicator">
          <b className="figure-text">Humidity: </b>
          <p className="figure-text">{data.day.avghumidity}%</p>
        </section>
        <section className="weather-indicator">
          <b className="figure-text">Visibility: </b>
          <p className="figure-text">{data.day.avgvis_km} km</p>
        </section>
        <section className="weather-indicator">
          <b className="figure-text">Chance of rain: </b>
          <p className="figure-text">{data.day.daily_chance_of_rain} %</p>
        </section>
        <section className="weather-indicator">
          <b className="figure-text">Chance of snow: </b>
          <p className="figure-text">{data.day.daily_chance_of_snow} %</p>
        </section>
        <section className="weather-indicator">
          <b className="figure-text">Moon phase: </b>
          <p className="figure-text">{data.astro.moon_phase}</p>
        </section>
      </article>
    </figure>
  );
}

export default Dashboard;
