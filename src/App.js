import logo from "./logo.svg";
import "./App.css";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  redirect,
} from "react-router-dom";
import { useCookies } from "react-cookie";
import Login from "./login/Login";
import Dashboard from "./dashboard/Dashboard";
function PrivateRoute() {
  const [cookies] = useCookies(["auth"]);

  if (!cookies.auth) {
    return <Navigate to="/" replace />;
  }

  return <Dashboard />;
}

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<PrivateRoute />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;
