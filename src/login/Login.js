import { useState, useRef } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

function Login() {
  //використовуємо хук для навігації
  const navigate = useNavigate();
  //задаємо пошту та пароль для входу
  const userEmail = "user1@ukr.net";
  const userPassword = "nastia12345";

  //стан для зберігання значень введених користувачем
  const [inputs, setInputs] = useState({});
  const [cookies, setCookie] = useCookies(["auth"]);
  const emailInput = useRef(null);
  const passwordInput = useRef(null);

  //обробник зміни вмісту введених полів
  function handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  }

  //обробник подання форми
  function handleSubmit(event) {
    event.preventDefault();
    if (inputs.email == userEmail && inputs.password == userPassword) {
      //правильні дані про входу
      emailInput.current.classList.remove("invalid");
      passwordInput.current.classList.remove("invalid");
      //встановлення кукі для позначення автентифікації
      setCookie("auth", true, { path: "/" });
      //перехід на сторінку dashboard
      navigate("/dashboard"); 
    } else {
      //неправильні дані для входу
      setCookie("auth", false, { path: "/" });

      //виводимо різні alert в залежності від того, що було введено неправильно
      if (inputs.email != userEmail && inputs.password != userPassword) {
        emailInput.current.classList.add("invalid");
        passwordInput.current.classList.add("invalid");
        alert("Incorrect email and password");
      } else if (inputs.email != userEmail) {
        emailInput.current.classList.add("invalid");
        alert("Incorrect email");
      } else {
        passwordInput.current.classList.add("invalid");
        alert("Incorrect password");
      }
    }
  }

  return (
    <div className="bg">
      <section className="form-container">
        <form className="form" onSubmit={handleSubmit}>
          <h1>Log to Web App</h1>
          <label className="label" htmlFor="email">
            E-mail:
            <input
              className="input"
              type="email"
              name="email"
              id="email"
              ref={emailInput}
              value={inputs.email || ""}
              onChange={handleChange}
            />
          </label>
          <label className="label" htmlFor="password">
            Password:
            <input
              className="input"
              type="password"
              name="password"
              id="password"
              ref={passwordInput}
              value={inputs.password || ""}
              onChange={handleChange}
            />
          </label>
          <button className="button-login" type="submit">
            Login
          </button>
        </form>
      </section>
    </div>
  );
}
export default Login;
